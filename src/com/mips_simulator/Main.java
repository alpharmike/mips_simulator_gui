package com.mips_simulator;

import com.mips_simulator.ALU.ALU;
import com.mips_simulator.Controls.ALUControlUnit;
import com.mips_simulator.Controls.ControlUnit;
import com.mips_simulator.Instruction.InstructionParser;
import com.mips_simulator.Memory.Byte;
//import com.mips_simulator.Memory.Memory;
import com.mips_simulator.Registers.Register;
import com.mips_simulator.Registers.DestSelector;
import com.mips_simulator.Registers.RegisterFile;
import com.mips_simulator.Registers.RegisterTable;
import com.mips_simulator.control.Simulator;
import simulator.gates.combinational.And;
//import simulator.gates.combinational.Memory;
import simulator.gates.combinational.ByteMemory;
import simulator.gates.combinational.Not;
import simulator.gates.combinational.Or;
import simulator.gates.sequential.ClockProvider;
import simulator.network.Link;
import simulator.wrapper.wrappers.*;
import simulator.wrapper.wrappers.nBit.Adder;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws Exception {
//        Adder adder = new Adder("ADDER", "10X6",
//                Simulator.falseLogic, Simulator.falseLogic, Simulator.trueLogic, Simulator.falseLogic, Simulator.trueLogic,
//                Simulator.falseLogic, Simulator.falseLogic, Simulator.falseLogic, Simulator.falseLogic, Simulator.trueLogic);
//
//        Mux4x1 mux = new Mux4x1("MUX", "6X1");
//        System.out.println(mux.getInput(0).getSignal());
//        mux.clearInput();
//        mux.addInput(Simulator.trueLogic, Simulator.falseLogic, Simulator.falseLogic, Simulator.falseLogic, Simulator.falseLogic, Simulator.trueLogic);
//        System.out.println(mux.getInput(0).getSignal());
//
//        Mux2x1 mux2x1 = new Mux2x1("mux2x1", "3X1", Simulator.falseLogic, Simulator.trueLogic, Simulator.falseLogic);
//
//        Mux8x1 mux8x1 = new Mux8x1("mux8x1", "11X1", Simulator.trueLogic, Simulator.trueLogic, Simulator.falseLogic, Simulator.falseLogic, Simulator.trueLogic, Simulator.trueLogic,
//                Simulator.trueLogic, Simulator.falseLogic, Simulator.trueLogic, Simulator.falseLogic, Simulator.falseLogic);
//        Mux16x1 mux16x1 = new Mux16x1("mux16x1", "20X1", Simulator.trueLogic, Simulator.trueLogic, Simulator.falseLogic, Simulator.falseLogic, Simulator.trueLogic, Simulator.trueLogic,
//                Simulator.trueLogic, Simulator.falseLogic, Simulator.trueLogic, Simulator.falseLogic, Simulator.falseLogic, Simulator.trueLogic, Simulator.trueLogic,
//                Simulator.trueLogic, Simulator.falseLogic, Simulator.trueLogic, Simulator.falseLogic, Simulator.falseLogic, Simulator.trueLogic, Simulator.falseLogic);
//        Decoder decoder = new Decoder("dec", "4X8", Simulator.trueLogic, Simulator.trueLogic, Simulator.falseLogic, Simulator.falseLogic);
//        DFlipFlop dFlipFlop = new DFlipFlop("d", ClockProvider.clock.getOutput(0), Simulator.trueLogic);


        // testing //

//        ControlUnit controlUnit = new ControlUnit("cu", "6X9", Simulator.falseLogic,
//                Simulator.falseLogic, Simulator.falseLogic, Simulator.trueLogic, Simulator.falseLogic, Simulator.falseLogic);
////
//        ALUControlUnit aluControlUnit = new ALUControlUnit("acu", "8X3", controlUnit.getOutput(7), controlUnit.getOutput(8), Simulator.trueLogic,
//                Simulator.falseLogic, Simulator.falseLogic, Simulator.falseLogic, Simulator.trueLogic, Simulator.falseLogic);

//        Simulator.debugger.addTrackItem(controlUnit);
//        Simulator.debugger.setDelay(200);
//        Simulator.circuit.startCircuit("real");
//
//
//        DestSelector destSelector = new DestSelector("destSel", "11X5", controlUnit.getOutput(0));
//        destSelector.addInput(Simulator.falseLogic);
//        destSelector.addInput(Simulator.falseLogic);
//        destSelector.addInput(Simulator.trueLogic);
//        destSelector.addInput(Simulator.trueLogic);
//        destSelector.addInput(Simulator.falseLogic);
//
//        destSelector.addInput(Simulator.falseLogic);
//        destSelector.addInput(Simulator.falseLogic);
//        destSelector.addInput(Simulator.falseLogic);
//        destSelector.addInput(Simulator.trueLogic);
//        destSelector.addInput(Simulator.trueLogic);
//
//        RegisterFile registerFile = new RegisterFile("regFile", "48X64");
//        registerFile.addInput(Simulator.falseLogic, Simulator.falseLogic, Simulator.falseLogic, Simulator.trueLogic, Simulator.trueLogic);
//        registerFile.addInput(Simulator.falseLogic, Simulator.falseLogic, Simulator.trueLogic, Simulator.trueLogic, Simulator.falseLogic);
//        registerFile.addInput(destSelector.getOutput(0), destSelector.getOutput(1), destSelector.getOutput(2), destSelector.getOutput(3), destSelector.getOutput(4));
//        for (int i = 0; i < 29; ++i) {
//            registerFile.addInput(Simulator.falseLogic);
//        }
//
//        registerFile.addInput(Simulator.trueLogic);
//        registerFile.addInput(Simulator.trueLogic);
//        registerFile.addInput(Simulator.trueLogic);
//        registerFile.addInput(Simulator.trueLogic);
//
//        ALU alu = new ALU("alu", "67X33", aluControlUnit.getOutput(0), aluControlUnit.getOutput(1), aluControlUnit.getOutput(2));
//        for (int i = 0; i < 64; ++i) {
//            alu.addInput(registerFile.getOutput(i));
//        }
//
//
//        LeftShifter leftShifter = new LeftShifter("ls", "37X32");
//        for (int i = 0; i < 30; ++i) {
//            leftShifter.addInput(Simulator.falseLogic);
//        }
//        leftShifter.addInput(Simulator.trueLogic);
//        leftShifter.addInput(Simulator.falseLogic);
//
//        leftShifter.addInput(Simulator.falseLogic);
//        leftShifter.addInput(Simulator.falseLogic);
//        leftShifter.addInput(Simulator.falseLogic);
//        leftShifter.addInput(Simulator.trueLogic);
//        leftShifter.addInput(Simulator.falseLogic);

//        RightShifter rightShifter = new RightShifter("rs", "37X32");
//        for (int i = 0; i < 29; ++i) {
//            rightShifter.addInput(Simulator.falseLogic);
//        }
//        rightShifter.addInput(Simulator.trueLogic);
//        rightShifter.addInput(Simulator.trueLogic);
//        rightShifter.addInput(Simulator.falseLogic);
//
//        rightShifter.addInput(Simulator.falseLogic);
//        rightShifter.addInput(Simulator.falseLogic);
//        rightShifter.addInput(Simulator.falseLogic);
//        rightShifter.addInput(Simulator.falseLogic);
//        rightShifter.addInput(Simulator.trueLogic);
//
//        Simulator.debugger.addTrackItem(rightShifter);
//        Simulator.debugger.setDelay(200);
//        Simulator.circuit.startCircuit();
//
//
//        RegisterTable.initialize();
//        ArrayList<ArrayList<Link>> instructions = InstructionParser.parse("./example/test2");
//
//        ControlUnit controlUnit = new ControlUnit("cu", "6X9");
//        ALUControlUnit aluControlUnit = new ALUControlUnit("acu", "8X3");
//        DestSelector destSelector = new DestSelector("destSel", "11X5", controlUnit.getOutput(0));
//        RegisterFile registerFile = new RegisterFile("regFile", "48X64");
//        ALU alu = new ALU("alu", "67X33", aluControlUnit.getOutput(0), aluControlUnit.getOutput(1), aluControlUnit.getOutput(2));


//        for (int i = 0; i < instructions.size(); ++i) {
//            for (int j = 0; j < 6; ++j) {
//                controlUnit.addInput(instructions.get(i).get(j));
//            }
//
//            aluControlUnit.addInput(controlUnit.getOutput(7), controlUnit.getOutput(8));
//            for (int j = 26; j < 32; ++j) {
//                aluControlUnit.addInput(instructions.get(i).get(j));
//            }
//            destSelector.addInput(controlUnit.getOutput(0));
//            for (int j = 11; j <= 20; ++j) {
//                destSelector.addInput(instructions.get(i).get(j));
//            }
//
//            for (int j = 6; j < 16; ++j) { // add rs and rt
//                registerFile.addInput(instructions.get(i).get(j));
//            }
//
//            for (int j = 0; j < destSelector.getOutputSize(); ++j) { // add write register
//                registerFile.addInput(destSelector.getOutput(j));
//            }
//
//            for (int j = 0; j < 32; ++j) { // add write data
//                registerFile.addInput(alu.getOutput(j + 1));
//            }
//
//            registerFile.addInput(controlUnit.getOutput(3)); // add reg write
//
////            for (int j = 0; j < aluControlUnit.getOutputSize(); ++j) {
////                alu.addInput(aluControlUnit.getOutput(j));
////            }
////
//            for (int j = 0; j < registerFile.getOutputSize(); ++j) {
//                alu.addInput(registerFile.getOutput(j));
//            }
//
//            Simulator.debugger.addTrackItem(controlUnit, aluControlUnit, destSelector, registerFile, alu);
//            Simulator.debugger.setDelay(200);
//            Simulator.circuit.startCircuit();
//
//            controlUnit.clearInput();
//            aluControlUnit.clearInput();
//            destSelector.clearInput();
//            registerFile.clearInput();
//            alu.clearInput();
//
//        }


//        Adder adder = new Adder("adder", "64X32");
//
//        Register pc = new Register("pc", "32X32");
//
//        for (int i = 0; i < 32; ++i) {
//            adder.addInput(pc.getOutput(i));
//        }
//
//        for (int i = 0; i < 29; ++i) {
//            adder.addInput(Simulator.falseLogic);
//        }
//
//        adder.addInput(Simulator.trueLogic);
//        adder.addInput(Simulator.falseLogic);
//        adder.addInput(Simulator.falseLogic);
//
//        for (int i = 0; i < 32; ++i) {
//            pc.addInput(adder.getOutput(i));
//        }


//        Memory memory = new Memory("mem", Simulator.falseLogic);
//
//        for (int i = 0; i < 16; ++i) {
//            memory.addInput(adder.getOutput(i + 16));
//        }


//        int i;
//        for (i = 0; i < 15; i++) {
//            memory.addInput(Simulator.falseLogic);
//        }
//        memory.addInput(Simulator.trueLogic);
//
//        for (i = 0; i < 29; i++) {
//            memory.addInput(Simulator.falseLogic);
//        }
//        memory.addInput(Simulator.trueLogic);
//        memory.addInput(Simulator.trueLogic);
//        memory.addInput(Simulator.trueLogic);


//        Adder adder = new Adder("adder", "64X32");
//        One one = new One("one", "0X32");
//        Register temp = new Register("temp", "32X32");
//        for (int i = 0; i < 32; ++i) {
//            temp.addInput(Simulator.falseLogic);
//            adder.addInput(Simulator.falseLogic);
//            adder.addInput(Simulator.falseLogic);
//
//        }
//
//        for (int i = 0; i < 32; ++i) {
//            temp.setInput(i, new Not("not", temp.getOutput(i)).getOutput(0));
//        }


//

//        for (int i = 0; i < 32; ++i) {
//            adder.setInput(i, temp.getOutput(i));
//        }
////
//        for (int i = 0; i < 32; ++i) {
//            adder.setInput(i + 32, one.getOutput(i));
//        }
//
//        for (int i = 0; i < 32; ++i) {
//            temp.setInput(i, adder.getOutput(i));
//        }

//        adder.addInput(Simulator.falseLogic);
//        adder.addInput(Simulator.falseLogic);
//        adder.addInput(Simulator.trueLogic);


//        for (int i = 0; i < 32; ++i) {
//            adder.addInput(adder.getOutput(i));
//        }
//
//        for (int i = 0; i < 29; ++i) {
//            adder.addInput(Simulator.falseLogic);
//        }
//
//        adder.setInput(61, Simulator.trueLogic);
//        adder.setInput(62, Simulator.falseLogic);
//        adder.setInput(63, Simulator.falseLogic);


//        InstructionMemory instructionMemory = new InstructionMemory("memory", "32X32");
//        for (int i = 0; i < 32; ++i) {
//            instructionMemory.addInput(pc.getOutput(i));
//        }
//
//        for (int i = 0; i < 32; ++i) {
//            adder.addInput(adder.getOutput(i));
//        }
//
//        for (int i = 0; i < 29; ++i) {
//            adder.addInput(Simulator.falseLogic);
//        }
//
//        adder.addInput(Simulator.trueLogic);
//        adder.addInput(Simulator.falseLogic);
//        adder.addInput(Simulator.falseLogic);
//
//        for (int i = 0; i < 32; ++i) {
//            pc.addInput(adder.getOutput(i));
//        }


//
//        Simulator.debugger.addTrackItem(temp, adder);
////
//        Simulator.debugger.setDelay(200);
////
//        Simulator.circuit.startCircuit();

//        Clock clock = new Clock("CLOCK", 900);


        RegisterTable.initialize();
        ArrayList<ArrayList<Link>> instructions = InstructionParser.parse("example/test");
        ArrayList<Link> initialInputs = new ArrayList<>();
        for (int i = 0; i < instructions.size(); ++i) {
            initialInputs.addAll(instructions.get(i));
        }

        Byte[] bytes = new Byte[initialInputs.size() / 8];
        for (int i = 0; i < initialInputs.size() / 8; ++i) {
            Byte newByte = new Byte();
            Boolean[] booleans = new Boolean[8];
            for (int j = 0; j < 8; ++j) {
                booleans[j] = initialInputs.get(i * 8 + j).getSignal();
            }
            newByte.setBits(booleans);
            bytes[i] = newByte;
        }




//        for (int i = 0; i < 32; ++i) {
//            adder.addInput(pc.getOutput(i));
//        }
//
//        for (int i = 0; i < 31; ++i) {
//            adder.addInput(Simulator.falseLogic);
//        }

//        adder.addInput(Simulator.trueLogic);

//        for (int i = 0; i < 32; ++i) {
//            pc.addInput(adder.getOutput(i));
//        }


        Register pc = new Register("pc", "32X32");
//        for (int i = 0; i < 32; ++i)
//            pc.addInput(Simulator.falseLogic);

        Link[] pcIn = new Link[32];
        for (int i = 0; i < 32; ++i)
            pcIn[i] = Simulator.falseLogic;

        Adder adder = new Adder("ADDER", "64X32");
        for (int i = 0; i < 32; ++i) {
            adder.addInput(pc.getOutput(i));
        }

        Link[] adderIn = new Link[32];
        for (int i = 0; i < 29; ++i)
            adderIn[i] = Simulator.falseLogic;
        adderIn[29] = Simulator.trueLogic;
        adderIn[30] = Simulator.falseLogic;
        adderIn[31] = Simulator.falseLogic;

        adder.addInput(adderIn);


        Mux2x1[] select = new Mux2x1[32];
        for (int i = 0; i < 32; ++i)
            select[i] = new Mux2x1("MUX" + i, "3X1");

        DFlipFlop[] shift = new DFlipFlop[2];
        shift[0] = new DFlipFlop("SH0", "2X2", ClockProvider.clock.getOutput(0), Simulator.falseLogic);
        shift[1] = new DFlipFlop("SH1", "2X2", ClockProvider.clock.getOutput(0), shift[0].getOutput(0));

        for (int i = 0; i < 32; ++i)
            select[i].addInput(shift[1].getOutput(0), adder.getOutput(i), pcIn[i]);

        for (int i = 0; i < 32; ++i)
            pc.addInput(select[i].getOutput(0));





//        Simulator.debugger.addTrackItem(pc);
//        Simulator.debugger.setDelay(200);
//        Simulator.circuit.startCircuit("real");


        ByteMemory instructionMemory = new ByteMemory("InstMem");
        SignExtend signExtend = new SignExtend("Offset", "16X32");
        ControlUnit controlUnit = new ControlUnit("cu", "6X9");
        ALUControlUnit aluControlUnit = new ALUControlUnit("acu", "8X3");
        DestSelector destSelector = new DestSelector("destSel", "11X5");
        RegisterFile registerFile = new RegisterFile("regFile", "48X64");
        LeftShifter leftShifter = new LeftShifter("Left Shifter", "37X32");
        Adder branchTargetAdder = new Adder("Branch Target", "64X32");
        DataSelector aluInputSelector = new DataSelector("ALU Source", "65X32");
        ALU alu = new ALU("alu", "67X33");
        Shifter shifter = new Shifter("shifter", "40X33");
        ALUOutputSelector aluOutputSelector = new ALUOutputSelector("ALU Output", "67X33");
        DataSelector pcSelector = new DataSelector("PC Source", "65X32");
        ByteMemory dataMemory = new ByteMemory("DataMem");
        DataSelector registerDataSelector = new DataSelector("Register Data", "65X32");



        // initializing the instruction memory with instructions
        Boolean[][] temp = new Boolean[65536][8];
        for (int i = 0; i < 65536; ++i) {
            for (int j = 0; j < 8; ++j) {
                temp[i][j] = false;
            }
        }

        for (int i = 0; i < initialInputs.size() / 8; ++i) {
            for (int j = 0; j < 8; ++j) {
                temp[i][j] = initialInputs.get(i * 8 + j).getSignal();
            }
        }

        instructionMemory.setMemory(temp);



        instructionMemory.addInput(Simulator.falseLogic);
        // set inputs for instruction memory, the 16 less significant bits used
        for (int i = 0; i < 16; ++i) {
            instructionMemory.addInput(pc.getOutput(i + 16));
        }

        for (int i = 0; i < 32; ++i) {
            instructionMemory.addInput(Simulator.falseLogic);
        }

        // sign extending 16 less significant bits of the instruction
        for (int i = 0; i < 16; ++i) {
            signExtend.addInput(instructionMemory.getOutput(i + 16));
        }

        // left shift the offset by two bits
        for (int i = 0; i < signExtend.getOutputSize(); ++i) {
            leftShifter.addInput(signExtend.getOutput(i));
        }

        leftShifter.addInput(Simulator.falseLogic);
        leftShifter.addInput(Simulator.falseLogic);
        leftShifter.addInput(Simulator.falseLogic);
        leftShifter.addInput(Simulator.trueLogic);
        leftShifter.addInput(Simulator.falseLogic);

        // calculating branch target address using an adder
        for (int i = 0; i < 32; ++i) {
            branchTargetAdder.addInput(pc.getOutput(i));
        }

        for (int i = 0; i < 32; ++i) {
            branchTargetAdder.addInput(leftShifter.getOutput(i));
        }

        // control unit
        for (int i = 0; i < 6; ++i) {
            controlUnit.addInput(instructionMemory.getOutput(i));
        }


        // alu control unit
        aluControlUnit.addInput(controlUnit.getOutput(7));
        aluControlUnit.addInput(controlUnit.getOutput(8));
        for (int i = 0; i < 6; ++i) {
            aluControlUnit.addInput(instructionMemory.getOutput(i + 26));
        }

        // register file destination selector(between rt and rd)
        destSelector.addInput(controlUnit.getOutput(0));
        for (int i = 11; i <= 20; ++i) {
            destSelector.addInput(instructionMemory.getOutput(i));
        }

        // reading data from register file
        for (int i = 6; i < 16; ++i) { // add rs and rt
            registerFile.addInput(instructionMemory.getOutput(i));
        }

        for (int i = 0; i < destSelector.getOutputSize(); ++i) { // add write register
            registerFile.addInput(destSelector.getOutput(i));
        }

        for (int i = 0; i < 32; ++i) { // add write data
            registerFile.addInput(registerDataSelector.getOutput(i));
        }

        registerFile.addInput(controlUnit.getOutput(3)); // add reg write

        // determine the second input of the alu, whether coming from register file or the immediate from the instruction
        aluInputSelector.addInput(controlUnit.getOutput(1));
        for (int i = 0; i < 32; ++i) {
            aluInputSelector.addInput(registerFile.getOutput(i + 32)); // the second output from regFile
        }

        for (int i = 0; i < 32; ++i) {
            aluInputSelector.addInput(signExtend.getOutput(i));
        }


        // alu and shift
        for (int i = 0; i < 3; ++i) {
            alu.addInput(aluControlUnit.getOutput(i));
        }

        for (int i = 0; i < 32; ++i) {
            alu.addInput(registerFile.getOutput(i));
        }

        for (int i = 0; i < 32; ++i) {
            alu.addInput(aluInputSelector.getOutput(i));
        }

        for (int i = 0; i < 32; ++i) {
            shifter.addInput(registerFile.getOutput(i + 32));
        }

        Or shamt = new Or("shamt");
        for (int i = 21; i <= 25; ++i) {
            shifter.addInput(instructionMemory.getOutput(i));
            shamt.addInput(instructionMemory.getOutput(i));
        }

        for (int i = 0; i < 3; ++i) {
            shifter.addInput(aluControlUnit.getOutput(i));
        }

        Not[] notOpcode = new Not[6];
        for (int i = 0; i < 6; ++i) {
            notOpcode[i] = new Not(String.format("not_%d", i), instructionMemory.getOutput(i));
        }

        And isShit = new And("shift");
        isShit.addInput(shamt.getOutput(0));
        for (int i = 0; i < 6; ++i) {
            isShit.addInput(notOpcode[i].getOutput(0));
        }

        // decide whether the output of arithmetic instruction should come from Shifter or ALU
        aluOutputSelector.addInput(isShit.getOutput(0));
        for (int i = 0; i < 33; ++i) {
            aluOutputSelector.addInput(alu.getOutput(i));
        }

        for (int i = 0; i < 33; ++i) {
            aluOutputSelector.addInput(shifter.getOutput(i));
        }

        // determine the value of pc using the zero output from alu result(used as PC Src signal)
        pcSelector.addInput(new And("pcSrc", controlUnit.getOutput(6), alu.getOutput(0)).getOutput(0));
        for (int i = 0; i < 32; ++i) {
            pcSelector.addInput(pc.getOutput(i));
        }

        for (int i = 0; i < 32; ++i) {
            pcSelector.addInput(branchTargetAdder.getOutput(i));
        }



        // data memory
        dataMemory.addInput(controlUnit.getOutput(5));
        for (int i = 0; i < 16; ++i) {
            dataMemory.addInput(aluOutputSelector.getOutput(i + 17)); // i + 17 because the first bit of alu output is the zero bit
        }

        for (int i = 0; i < 32; ++i) {
            dataMemory.addInput(registerFile.getOutput(i + 32));
        }

        // determine the value to write into the write register
        registerDataSelector.addInput(controlUnit.getOutput(2));
        for (int i = 0; i < 32; ++i) {
            registerDataSelector.addInput(aluOutputSelector.getOutput(i + 1));
        }

        for (int i = 0; i < 32; ++i) {
            registerDataSelector.addInput(dataMemory.getOutput(i));
        }


        Simulator.debugger.addTrackItem(pc, instructionMemory, controlUnit, aluControlUnit, registerFile, aluOutputSelector, dataMemory);
//        Simulator.debugger.addTrackItem(instructionMemory, signExtend, branchTargetAdder, controlUnit, aluControlUnit, destSelector, registerFile, alu);
        Simulator.debugger.setDelay(200);
//        Simulator.circuit.startCircuit();






//false true true true true false false false false
//false true false false false true false false false


//        instructionMemory.addInput(ClockProvider.clock.getOutput(0));
//        for (int i = 0; i < 16; i++) { // address = 0
//            instructionMemory.addInput(pc.getOutput(i + 16));
////            instructionMemory.addInput(Simulator.falseLogic);
//        }
////
//        for (int i = 0; i < 31; ++i) {
//            instructionMemory.addInput(Simulator.trueLogic);
//        }
//        instructionMemory.addInput(Simulator.falseLogic);

//        instructionMemory.addInput(ClockProvider.clock.getOutput(0));


//        System.out.println(instructionMemory.getInputs().size());
//
//        Simulator.debugger.addTrackItem(pc, instructionMemory);
//        Simulator.debugger.setDelay(200);
//        Simulator.circuit.startCircuit("real");


    }

}
