package com.mips_simulator.Registers;

import simulator.gates.sequential.ClockProvider;
import simulator.network.Link;
import simulator.wrapper.Wrapper;
import simulator.wrapper.wrappers.DFlipFlop;

import java.util.ArrayList;

public class Register extends Wrapper {
    private ArrayList<DFlipFlop> bits;
    public Register(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        if (bits == null) {
            bits = new ArrayList<>();
            for (int i = 0; i < 32; ++i) {
                simulator.wrapper.wrappers.DFlipFlop dFlipFlop = new simulator.wrapper.wrappers.DFlipFlop(String.format("d_flipflop_%d", i), "2X2", ClockProvider.clock.getOutput(0), getInput(i));
                bits.add(dFlipFlop);
            }
        } else {
            for (int i = 0; i < 32; ++i) {
                bits.get(i).setInput(1, getInput(i));
            }
        }

        for (int i = 0; i < 32; ++i) {
            addOutput(this.bits.get(i).getOutput(0));
        }

    }

    public ArrayList<DFlipFlop> getBits() {
        return bits;
    }

    @Override
    public String toString() {
        StringBuilder value = new StringBuilder("");
        getOutputs().forEach(bit -> {
            value.append(bit.getSignal() ? "1" : "0");
        });

        return value.toString();
    }
}
