package com.mips_simulator.Registers;

import com.mips_simulator.control.Simulator;
import simulator.network.Link;
import simulator.wrapper.Wrapper;
import simulator.wrapper.wrappers.Decoder;
import simulator.wrapper.wrappers.Mux2x1;
import simulator.wrapper.wrappers.Mux32x1;

import java.util.ArrayList;
import java.util.List;

public class RegisterFile extends Wrapper {

    private static ArrayList<Register> registers = new ArrayList<>();

    public RegisterFile(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        if (registers == null || registers.isEmpty()) {
            registers = new ArrayList<>();
            for (int i = 0; i < 32; ++i) {
                Register register = new Register(String.format("register_%d", i), "32X32");
                for (int j = 0; j < 32; ++j) {
                    register.addInput(Simulator.falseLogic);
                }
                registers.add(register);
            }
        }
        List<Link> rr1 = getInputs(0, 5);
        List<Link> rr2 = getInputs(5, 10);
        List<Link> wr = getInputs(10, 15);
        List<Link> wd = getInputs(15, 47);
        Link regWrite = getInput(47);

        // write data
        Decoder decoder = new Decoder("decoder", "6X32", regWrite);
        for (int i = 0; i < 5; ++i) {
            decoder.addInput(wr.get(i));
        }

        for (int i = 0; i < decoder.getOutputSize(); ++i) {
            for (int j = 0; j < 32; ++j) {
                Mux2x1 mux2x1 = new Mux2x1("mux2x1", "3X1", decoder.getOutput(j), registers.get(j).getOutput(i), wd.get(i));
                registers.get(j).setInput(i, mux2x1.getOutput(0));
            }
        }

        // read data 1
        ArrayList<Mux32x1> rd1Mux = new ArrayList<>();

        for (int i = 0; i < 32; ++i) {
            Mux32x1 mux32x1 = new Mux32x1(String.format("mux32x1_%d", i), "37X1");
            for (int j = 0; j < 5; ++j) {
                mux32x1.addInput(rr1.get(j));
            }

            for (int j = 0; j < 32; ++j) {
                mux32x1.addInput(registers.get(j).getOutput(i));
            }

            rd1Mux.add(mux32x1);
        }


        //read data 2
        ArrayList<Mux32x1> rd2Mux = new ArrayList<>();

        for (int i = 0; i < 32; ++i) {
            Mux32x1 mux32x1 = new Mux32x1(String.format("mux32x1_%d", i), "37X1");
            for (int j = 0; j < 5; ++j) {
                mux32x1.addInput(rr2.get(j));
            }

            for (int j = 0; j < 32; ++j) {
                mux32x1.addInput(registers.get(j).getOutput(i));
            }

            rd2Mux.add(mux32x1);
        }

        // return read data 1 and read data 2 as output

        for (int i = 0; i < 32; ++i) {
            addOutput(rd1Mux.get(i).getOutput(0));
        }

        for (int i = 0; i < 32; ++i) {
            addOutput(rd2Mux.get(i).getOutput(0));
        }
    }

    public static ArrayList<Register> getRegisters() {
        return registers;
    }
}
