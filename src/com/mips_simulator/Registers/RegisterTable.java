package com.mips_simulator.Registers;

import java.util.HashMap;
import java.util.Map;

public abstract class RegisterTable {
    protected static Map<String, Integer> registers = new HashMap<>();

    public RegisterTable() {

    }

    public static void initialize() {
        if (registers == null || registers.isEmpty()) {
            registers = new HashMap<>();
            registers.put("$zero", 0x00);
            registers.put("$at", 0x01);
            registers.put("$v0", 0x02);
            registers.put("$v1", 0x03);
            registers.put("$a0", 0x04);
            registers.put("$a1", 0x05);
            registers.put("$a2", 0x06);
            registers.put("$a3", 0x07);
            registers.put("$t0", 0x08);
            registers.put("$t1", 0x09);
            registers.put("$t2", 0x0A);
            registers.put("$t3", 0x0B);
            registers.put("$t4", 0x0C);
            registers.put("$t5", 0x0D);
            registers.put("$t6", 0x0E);
            registers.put("$t7", 0x0F);
            registers.put("$s0", 0x10);
            registers.put("$s1", 0x11);
            registers.put("$s2", 0x12);
            registers.put("$s3", 0x13);
            registers.put("$s4", 0x14);
            registers.put("$s5", 0x15);
            registers.put("$s6", 0x16);
            registers.put("$s7", 0x17);
            registers.put("$t8", 0x18);
            registers.put("$t9", 0x19);
            registers.put("$k0", 0x1A);
            registers.put("$k1", 0x1B);
            registers.put("$gp", 0x1C);
            registers.put("$sp", 0x1D);
            registers.put("$fp", 0x1E);
            registers.put("$ra", 0x1F);
        }
    }

    public static Map<String, Integer> getRegisters() {
        return registers;
    }
}
