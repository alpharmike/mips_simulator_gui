package com.mips_simulator.Registers;

import simulator.network.Link;
import simulator.wrapper.Wrapper;
import simulator.wrapper.wrappers.Mux2x1;

import java.util.ArrayList;
import java.util.List;

public class DestSelector extends Wrapper {
    public DestSelector(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        Link regDst = getInput(0);
        List<Link> rt = getInputs(1, 6);

        List<Link> rd = getInputs(6, 11);

        ArrayList<Mux2x1> destMux = new ArrayList<>();

        for (int i = 0; i < 5; ++i) {
            Mux2x1 mux2x1 = new Mux2x1(String.format("mux_%d", i), "3X1", regDst, rt.get(i), rd.get(i));
            destMux.add(mux2x1);
        }

        for (int i = 0; i < 5; ++i) {
            addOutput(destMux.get(i).getOutput(0));
        }
    }
}
