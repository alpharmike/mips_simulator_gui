package com.mips_simulator.Memory;

import com.mips_simulator.control.Simulator;
import simulator.gates.sequential.ClockProvider;
import simulator.network.Link;
import simulator.network.Node;
import simulator.wrapper.wrappers.DFlipFlop;

import java.util.List;

public class Memory extends Node {
    private Byte[] bytes;

    // inputs 0-15 are address bits
    // inputs 16-47 are data bits
    // input 48 is MemWrite signal

    public Memory(String label, Link... links) {
        super(label, links);
        this.bytes = new Byte[65536];
        for (int i = 0; i < 65536; ++i) {
            bytes[i] = new Byte();
        }

        for (int i = 0; i < 32; ++i) {
            addOutputLink(false);
        }
    }

    private int address() {
        int i, address = 0;
        for (i = 0; i < 16; ++i) {
            if (inputs.size() > i) {
                if (getInput(i).getSignal()) {
                    address += Math.pow(2, 15 - i);
                }
            }
        }

        if (address <= 0 || address >= 65535) {
            return 0;
        }

        return address;
    }


    private void memoryWrite() {
        int i, j, address = address();
        // Writing 4 bytes
        for (i = 0; i < 4; i++) {
            if (this.bytes[address + i] == null) { // This byte is not used before.
                Byte b = new Byte();
                this.bytes[address + i] = b;
            }
            // Writing 8 bits on each byte
            for (j = 0; j < 8; j++) {
                Link memInput = this.getInput(i * 8 + j + 16);
                this.bytes[address + i].setBit(j, memInput.getSignal());
                this.setOutput(i * 8 + j, memInput);
            }
        }
    }

    public void memoryRead() {
        int i, j, address = address();
        // Reading 4 bytes
        for (i = 0; i < 4; ++i) {
            if (this.bytes[address + i] == null) { // This byte is not used before.
                Byte b = new Byte();
                this.bytes[address + i] = b;
            }

            // Reading 8 bits of each byte
            for (j = 0; j < 8; ++j) {
                this.setOutput(i * 8 + j, this.bytes[address + i].getBit(j) ? Simulator.trueLogic : Simulator.falseLogic);
            }
        }

    }


    @Override
    public void evaluate() {

        if (this.getInput(48).getSignal()) {
            //System.out.println("Write");
            memoryWrite();
        } else {
            //System.out.println("Read");
            memoryRead();
        }
    }

    public Byte[] getBytes() {
        return bytes;
    }

    public void setBytes(Byte[] bytes) {
        int i;
        for (i = 0; i < bytes.length; i++){
            this.bytes[i] = bytes[i];
        }

        for (i = 0; i < 4; ++i) {
            for (int j = 0; j < 8; ++j) {
//                setOutputLink(i * 8 + j, this.bytes[i].getBit(j));
            }
        }
    }

    @Override
    public String toString() {
        String s = "";
        int i = 0;
        for (i = 0; i < 20; i++) {
            if (this.bytes[i] != null)
                s += Integer.toString(i) + ") " + this.bytes[i].toString() + "\n";
            else
                s += Integer.toString(i) + ") null\n";
        }
        return s;
    }

    @Override
    public List<Link> getInputs(int starIndex, int endIndex) {
        return null;
    }
}
