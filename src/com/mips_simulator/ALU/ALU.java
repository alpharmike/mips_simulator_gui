package com.mips_simulator.ALU;

import com.mips_simulator.control.Simulator;
import simulator.gates.combinational.Not;
import simulator.gates.combinational.Or;
import simulator.network.Link;
import simulator.wrapper.Wrapper;
import simulator.wrapper.wrappers.Mux8x1;
import simulator.wrapper.wrappers.nBit.*;

// 67X33
public class ALU extends Wrapper {
    public ALU(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        //first 3 bits of ALU input are mux8to1 select signals//
        //the last bit of the of ALU output is Zero bit//

        Adder adder = new Adder("Adder", "64X32");
        Subtractor subtractor = new Subtractor("Subtractor", "64X32");
        NBitAnd nBitAnd = new NBitAnd("NBitAnd", "64X32");
        NBitOr nBitOr = new NBitOr("NBitOr", "64X32");
        NBitXor nBitXor = new NBitXor("NBitXor", "64X32");
        NBitNor nBitNor = new NBitNor("NBitNor", "64X32");
        SetOnLessThan setOnLessThan = new SetOnLessThan("SetOnLessThan", "1X32");

        // and: 000
        // or: 001
        // add: 010
        // xor: 100
        // nor: 101
        // sub: 110
        // slt: 111

        //add 64 ALU inputs to all operators//
        for (int i = 0; i < 64; i++) {
            adder.addInput(getInput(i + 3));
            subtractor.addInput(getInput(i + 3));
            nBitAnd.addInput(getInput(i + 3));
            nBitOr.addInput(getInput(i + 3));
            nBitXor.addInput(getInput(i + 3));
            nBitNor.addInput(getInput(i + 3));
        }
        setOnLessThan.addInput(subtractor.getOutput(0));

        //initialize all muxes and pass our 3 bit select signals(the output of alu control unit) to them//
        Mux8x1[] mux8x1s = new Mux8x1[32];
        for (int i = 0; i < 32; i++) {
            mux8x1s[i] = new Mux8x1("mux" + i, "11X1", getInput(0), getInput(1), getInput(2));
        }

        //add all outputs of the operations to the mux//
        //according to the ALU Control numbers//
        for (int i = 0; i < 32; i++) {
            mux8x1s[i].addInput(nBitAnd.getOutput(i), nBitOr.getOutput(i), adder.getOutput(i), Simulator.falseLogic,
                    nBitXor.getOutput(i), nBitNor.getOutput(i), subtractor.getOutput(i), setOnLessThan.getOutput(i));
        }

        //setting Zero bit//
        Or or0 = new Or("or0");
        Not not0 = new Not("not0");
        for (int i = 0; i < 32; i++)
            or0.addInput(mux8x1s[i].getOutput(0));
        not0.addInput(or0.getOutput(0));
        addOutput(not0.getOutput(0));

        //put mux output on output signals//
        for (int i = 0; i < 32; i++) {
            addOutput(mux8x1s[i].getOutput(0));
        }

    }
}
