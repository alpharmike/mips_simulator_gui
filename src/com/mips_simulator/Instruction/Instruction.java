package com.mips_simulator.Instruction;

import com.mips_simulator.Registers.RegisterTable;
import com.mips_simulator.Utils.Padding;
import com.mips_simulator.control.Simulator;
import simulator.network.Link;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class Instruction {
    protected static Map<InstructionFormat, ArrayList<String>> instructionTable;
    protected static Map<String, Long> opcodes;
    protected static Map<String, Long> functs;
    private String representation;
    protected String opcode;
    protected String rs;
    protected String rt;
    protected String rd;
    protected String shamt;
    protected String imm;
    protected String funct;
    protected ArrayList<String> tokenized;
    protected InstructionFormat format;
    protected String encoded;
    protected ArrayList<Link> logicFormat;

    public Instruction(String representation) {
        this.representation = representation;

        this.initialize();
        this.tokenize();
        this.setOpcode();
        this.setFormat();
        this.setFunct();
        this.parseRegisters();
        this.setShamt();
        this.setImmediate();
        this.encode();
        this.convertToLogicalFormat();
    }


    public void initialize() {
        if (opcodes == null) {
            opcodes = new HashMap<>();
            opcodes.put("add", 0x00L);
            opcodes.put("sub", 0x00L);
            opcodes.put("addi", 0x08L);
            opcodes.put("andi", 0x0CL);
            opcodes.put("and", 0x00L);
            opcodes.put("or", 0x00L);
            opcodes.put("xor", 0x00L);
            opcodes.put("nor", 0x00L);
            opcodes.put("sll", 0x00L);
            opcodes.put("srl", 0x00L);
            opcodes.put("beq", 0x04L);
            opcodes.put("lw", 0x23L);
            opcodes.put("sw", 0x2BL);
        }

        if (functs == null) {
            functs = new HashMap<>();
            functs.put("add", 0x20L);
            functs.put("sub", 0x22L);
            functs.put("and", 0x24L);
            functs.put("or", 0x25L);
            functs.put("xor", 0x26L);
            functs.put("nor", 0x27L);
            functs.put("sll", 0x00L);
            functs.put("srl", 0x02L);
        }

        if (instructionTable == null) {
            instructionTable = new HashMap<>();
            ArrayList<String> RTypes = new ArrayList<>();
            RTypes.add("add");
            RTypes.add("sub");
            RTypes.add("xor");
            RTypes.add("or");
            RTypes.add("and");
            RTypes.add("sll");
            RTypes.add("srl");

            ArrayList<String> ITypes = new ArrayList<>();
            ITypes.add("addi");
            ITypes.add("beq");
            ITypes.add("ori");

            ArrayList<String> JTypes = new ArrayList<>();
            instructionTable.put(InstructionFormat.R_Type, RTypes);
            instructionTable.put(InstructionFormat.I_Type, ITypes);
            instructionTable.put(InstructionFormat.J_Type, JTypes);
        }
    }

    private void tokenize() {
        String instructionLine = this.representation;
        instructionLine = instructionLine.replaceAll(",", "");
        StringTokenizer tokens = new StringTokenizer(instructionLine, " ");
        ArrayList<String> tokenizedInstruction = new ArrayList<>();
        while (tokens.hasMoreTokens()) {
            tokenizedInstruction.add(tokens.nextToken());
        }

        this.tokenized = tokenizedInstruction;
    }

    private void setOpcode() {
        this.opcode = Padding.padLeft(Long.toBinaryString(opcodes.get(this.tokenized.get(0))), 6);
    }

    private void setFormat() {
        String instruction = this.tokenized.get(0);
        if (instructionTable.get(InstructionFormat.R_Type).contains(instruction)) {
            this.format = InstructionFormat.R_Type;
        } else if (instructionTable.get(InstructionFormat.I_Type).contains(instruction)) {
            this.format = InstructionFormat.I_Type;
        } else if (instructionTable.get(InstructionFormat.J_Type).contains(instruction)) {
            this.format = InstructionFormat.J_Type;
        }
    }

    private void setFunct() {
        String instruction = this.tokenized.get(0);
        this.funct = functs.containsKey(instruction) ? Padding.padLeft(Long.toBinaryString(functs.get(instruction)), 6) : "";
    }

    private void setImmediate() {
        if (Long.parseLong(this.opcode, 2) == opcodes.get("lw") || Long.parseLong(this.opcode, 2) == opcodes.get("sw")) {
            this.imm = Padding.padLeft(Integer.toBinaryString(Integer.parseInt(this.parseWrappedOffset(), 10)), 16);
        } else if (this.format == InstructionFormat.I_Type) {
            this.imm = Padding.padLeft(Integer.toBinaryString(Integer.parseInt(this.parseOffset(), 10)), 16);

        }
    }

    private void setShamt() {
        try {
            boolean leftShift = Long.parseLong(this.funct, 2) == functs.get("sll") && Long.parseLong(this.opcode, 2) == opcodes.get("sll");
            boolean rightShift = Long.parseLong(this.funct, 2) == functs.get("srl") && Long.parseLong(this.opcode, 2) == opcodes.get("srl");
            if (rightShift || leftShift) {
                this.shamt = Padding.padLeft(Integer.toBinaryString(Integer.parseInt(this.tokenized.get(3))), 5);
            }

            else if (this.format == InstructionFormat.R_Type) {
                this.shamt = Padding.padLeft("0", 5);
            }
        } catch (Exception e) {
            if (this.format == InstructionFormat.I_Type) {
                return;
            }
        }
    }

    private void parseRegisters() {
        try {
            boolean leftShift = Long.parseLong(this.funct, 2) == functs.get("sll") && Long.parseLong(this.opcode, 2) == opcodes.get("sll");
            boolean rightShift = Long.parseLong(this.funct, 2) == functs.get("srl") && Long.parseLong(this.opcode, 2) == opcodes.get("srl");

            if (leftShift || rightShift) {
                this.rs = Padding.padLeft("0", 5);
                this.rt = Padding.padLeft(Integer.toBinaryString(RegisterTable.getRegisters().get(this.tokenized.get(2))), 5);
                this.rd = Padding.padLeft(Integer.toBinaryString(RegisterTable.getRegisters().get(this.tokenized.get(1))), 5);
            } else if (this.format == InstructionFormat.R_Type) {
                this.rs = Padding.padLeft(Integer.toBinaryString(RegisterTable.getRegisters().get(this.tokenized.get(2))), 5);
                this.rt = Padding.padLeft(Integer.toBinaryString(RegisterTable.getRegisters().get(this.tokenized.get(3))), 5);
                this.rd = Padding.padLeft(Integer.toBinaryString(RegisterTable.getRegisters().get(this.tokenized.get(1))), 5);
            }

        } catch (Exception e) {
            if (Long.parseLong(this.opcode, 2) == opcodes.get("lw") || Long.parseLong(this.opcode, 2) == opcodes.get("sw")) {
                this.rs = Padding.padLeft(Integer.toBinaryString(RegisterTable.getRegisters().get(this.parseWrappedRegister())), 5);
                this.rt = Padding.padLeft(Integer.toBinaryString(RegisterTable.getRegisters().get(this.tokenized.get(1))), 5);
            } else if (this.format == InstructionFormat.I_Type) {
                this.rs = Padding.padLeft(Integer.toBinaryString(RegisterTable.getRegisters().get(this.tokenized.get(2))), 5);
                this.rt = Padding.padLeft(Integer.toBinaryString(RegisterTable.getRegisters().get(this.tokenized.get(1))), 5);
            }
        }


    }

    private String parseWrappedRegister() {
        return this.representation.substring(this.representation.indexOf('(') + 1, this.representation.indexOf(')'));
    }

    private String parseWrappedOffset() {
        return this.tokenized.get(2).substring(0, this.tokenized.get(2).indexOf('('));
    }

    private String parseOffset() {
        String offset = "";
        if (this.format == InstructionFormat.I_Type) {
            offset = this.tokenized.get(3);
        }
        return offset;
    }

    private void encode() {
        StringBuilder encoded = new StringBuilder();
        encoded.append(this.opcode);
        if (this.format == InstructionFormat.R_Type) {
            encoded.append(this.rs);
            encoded.append(this.rt);
            encoded.append(this.rd);
            encoded.append(this.shamt);
            encoded.append(this.funct);
        } else if (this.format == InstructionFormat.I_Type || Long.parseLong(this.opcode, 2) == opcodes.get("lw") || Long.parseLong(this.opcode, 2) == opcodes.get("sw")) {
            encoded.append(this.rs);
            encoded.append(this.rt);
            encoded.append(this.imm);
        }

        this.encoded = encoded.toString();
    }

    private void convertToLogicalFormat() {
        System.out.println(this.encoded);
        ArrayList<Link> bits = new ArrayList<>();
        for (int i = 0; i < 32; ++i) {
            if (this.encoded.charAt(i) == '1') {
                bits.add(Simulator.trueLogic);
            } else {
                bits.add(Simulator.falseLogic);
            }
        }

        this.logicFormat = bits;
    }

    public String hexRepresentation() {
        if(this.format == InstructionFormat.R_Type) {
            return String.format("%-22s (FUNCT:0x%x, RS:0x%x, RT:0x%x, RD:0x%x)",
                    representation, Integer.parseInt(funct, 2), Integer.parseInt(rs, 2), Integer.parseInt(rt, 2), Integer.parseInt(rd, 2));
        } else {
            return String.format("%-22s (OP:0x%x, RS:0x%x, RT:0x%x, IMM:0x%x)",
                    representation, Integer.parseInt(opcode, 2), Integer.parseInt(rs, 2), Integer.parseInt(rt, 2), Integer.parseInt(imm, 2));
        }

    }

    public String getRepresentation() {
        return representation;
    }

    public String getOpcode() {
        return opcode;
    }

    public String getRs() {
        return rs;
    }

    public String getRt() {
        return rt;
    }

    public String getRd() {
        return rd;
    }

    public String getShamt() {
        return shamt;
    }

    public String getImm() {
        return imm;
    }

    public String getFunct() {
        return funct;
    }

    public ArrayList<String> getTokenized() {
        return tokenized;
    }

    public InstructionFormat getFormat() {
        return format;
    }

    public String getEncoded() {
        return encoded;
    }

    public ArrayList<Link> getLogicFormat() {
        return logicFormat;
    }
}
