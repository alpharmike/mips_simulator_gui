package com.mips_simulator.Instruction;

import simulator.network.Link;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class InstructionParser {

    public static ArrayList<ArrayList<Link>> parse(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        Scanner scanner = new Scanner(file);

        ArrayList<String> instructionLines = new ArrayList<>();

        String currLine;
        while (scanner.hasNextLine() && !(currLine = scanner.nextLine()).isEmpty()) {
            instructionLines.add(currLine);
        }

        ArrayList<ArrayList<Link>> instructions = new ArrayList<>();

        for (String instructionLine : instructionLines) {
            Instruction instruction = new Instruction(instructionLine);
            System.out.println(instruction.getEncoded());
            instructions.add(instruction.getLogicFormat());
        }

        return instructions;

    }

    public static ArrayList<String> parseLines(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        Scanner scanner = new Scanner(file);

        ArrayList<String> instructionLines = new ArrayList<>();

        String currLine;
        while (scanner.hasNextLine() && !(currLine = scanner.nextLine()).isEmpty()) {
            instructionLines.add(currLine);
        }

//        ArrayList<ArrayList<Link>> instructions = new ArrayList<>();
//
//        for (String instructionLine : instructionLines) {
//            Instruction instruction = new Instruction(instructionLine);
//            System.out.println(instruction.getEncoded());
//            instructions.add(instruction.getLogicFormat());
//        }

        return instructionLines;

    }
}
