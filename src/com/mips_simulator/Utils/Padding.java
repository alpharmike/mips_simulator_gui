package com.mips_simulator.Utils;

public class Padding {
    public static String padLeft(String value, int length) {
        if (value.length() > length) {
            return value.substring(value.length() - length, value.length());
        }
        int zeroes = length - value.length();
        StringBuilder finalString = new StringBuilder();
        for (int i = 0; i < zeroes; ++i) {
            finalString.append("0");
        }
        finalString.append(value);
        return finalString.toString();
    }
}
