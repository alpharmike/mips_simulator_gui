package simulator.wrapper.wrappers;

import simulator.network.Link;
import simulator.wrapper.Wrapper;
import simulator.wrapper.wrappers.Mux2x1;

import java.util.ArrayList;
import java.util.List;

public class DataSelector extends Wrapper {
    public DataSelector(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        // first bit is the ALU Src signal indicating what to choose as the input for ALU
        // first 32-bit data comes from register file
        // second 32-bit data is the sign-extended immediate

        // first bit is the PC Src signal indicating what to choose as value for PC
        // first 32-bit data is PC + 4
        // second 32-bit data is the sign-extended offset from the BEQ instruction

        // first bit is the MemToReg signal indicating what to write in register
        // first 32-bit data comes from the result of the ALU
        // second 32-bit data comes from data memory

        Link signal = getInput(0);
        List<Link> data1 = getInputs(1, 33);
        List<Link> data2 = getInputs(33, 65);

        ArrayList<Mux2x1> bitSelectors = new ArrayList<>();

        for (int i = 0; i < 32; ++i) {
            Mux2x1 mux2x1 = new Mux2x1(String.format("mux_%d", i), "3X1", signal, data1.get(i), data2.get(i));
            bitSelectors.add(mux2x1);
        }

        for (int i = 0; i < 32; ++i) {
            addOutput(bitSelectors.get(i).getOutput(0));
        }
    }
}
