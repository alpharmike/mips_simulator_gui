package simulator.wrapper.wrappers;

import simulator.gates.combinational.And;
import simulator.gates.combinational.Not;
import simulator.gates.combinational.Or;
import simulator.network.Link;
import simulator.wrapper.Wrapper;

public class Mux2x1 extends Wrapper {
    public Mux2x1(String label, String stream, Link... links) {
        super(label, stream, links);
    }

    @Override
    public void initialize() {
        Link s = getInput(0);
        Link d1 = getInput(1);
        Link d0 = getInput(2);

        Not notS = new Not("not_s", s);

        And and0 = new And("and0", s, d0);
        And and1 = new And("and1", notS.getOutput(0), d1);

        Or or0 = new Or("or0", and0.getOutput(0), and1.getOutput(0));

        addOutput(or0.getOutput(0));
    }
}
