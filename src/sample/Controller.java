package sample;

import com.mips_simulator.ALU.ALU;
import com.mips_simulator.Controls.ALUControlUnit;
import com.mips_simulator.Controls.ControlUnit;
import com.mips_simulator.Instruction.Instruction;
import com.mips_simulator.Instruction.InstructionParser;
import com.mips_simulator.Registers.DestSelector;
import com.mips_simulator.Registers.Register;
import com.mips_simulator.Registers.RegisterFile;
import com.mips_simulator.Registers.RegisterTable;
import com.mips_simulator.control.Simulator;
import simulator.control.StackFrame;
import simulator.gates.combinational.*;
import simulator.gates.sequential.BigClock;
import simulator.gates.sequential.Clock;
import simulator.gates.sequential.ClockProvider;
import simulator.network.Link;
import simulator.network.Node;
import simulator.wrapper.DataStream;
import simulator.wrapper.wrappers.*;
import simulator.wrapper.wrappers.nBit.Adder;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import javax.swing.DefaultListModel;
import javax.swing.SwingUtilities;

public class Controller implements Runnable {
    private GUI gui;
    private List<Instruction> instructions;

    RegisterFile registerFile;
//    ByteMemory instructionMemory = new ByteMemory("InstMem");
//    SignExtend signExtend = new SignExtend("Offset", "16X32");
//    ControlUnit controlUnit = new ControlUnit("cu", "6X9");
//    ALUControlUnit aluControlUnit = new ALUControlUnit("acu", "8X3");
//    DestSelector destSelector = new DestSelector("destSel", "11X5");
//    RegisterFile registerFile = new RegisterFile("regFile", "48X64");
//    LeftShifter leftShifter = new LeftShifter("Left Shifter", "37X32");
//    Adder branchTargetAdder = new Adder("Branch Target", "64X32");
//    DataSelector aluInputSelector = new DataSelector("ALU Source", "65X32");
//    ALU alu = new ALU("alu", "67X33");
//    Shifter shifter = new Shifter("shifter", "40X33");
//    ALUOutputSelector aluOutputSelector = new ALUOutputSelector("ALU Output", "67X33");
//    DataSelector pcSelector = new DataSelector("PC Source", "65X32");
//    ByteMemory dataMemory = new ByteMemory("DataMem");
//    DataSelector registerDataSelector = new DataSelector("Register Data", "65X32");

    private List<DataStream> dataStreams;
    private List<Clock> clocks;
    private List<List<Node>> netList;
    private Map<Link, List<Node>> removed;
    private Thread thread;
    private List<BigClock> bigClocks;
    private int clockCount;
    private List<Memory> memories;
    private List<ByteMemory> byteMemories;

    private DefaultListModel instructionModel;
    private DefaultListModel registerModel;
    private DefaultListModel memoryModel;

    private volatile boolean running = true;
    private boolean hexadecimal = false;

    public Controller() {
        gui = new GUI();
        gui.setGUIListener(listener);

        dataStreams = new ArrayList<>();
        removed = new HashMap<>();
        netList = new ArrayList<>();
        netList.add(new ArrayList<>());
        clocks = new ArrayList<>();
        thread = new Thread(this);
        bigClocks = new ArrayList<>();
        memories = new ArrayList<>();
        byteMemories = new ArrayList<>();
        clockCount = -1;
        RegisterTable.initialize();

        instructionModel = new DefaultListModel();
        gui.setInstructionListModel(instructionModel);

        registerModel = new DefaultListModel();
        gui.setRegisterListModel(registerModel);

        memoryModel = new DefaultListModel();
        gui.setMemoryListModel(memoryModel);
    }

    /**
     * Refresh the interface with the current processor state
     */
    private void refresh() {


    }

    private String string_value(int b) {
        if (hexadecimal) {
            return String.format("0x%x", b & 0xffffffffL);
        } else {
            return String.format("%d", b & 0xffffffffL);
        }
    }

//    private void refreshLater() {
//        SwingUtilities.invokeLater(new Runnable() {
//            @Override
//            public void run() {
//                refresh();
//            }
//        });
//    }

    /**
     * Run the simulation until it ends or the user stops
     */
//    private void run() {
//        if (running) {
//            return;
//        }
//        new Thread() {
//            @Override
//            public void run() {
//                if (running) {
//                    return;
//                }
//                running = true;
//                while (running) {
//                    //
//                }
//                refreshLater();
//            }
//
//            ;
//        }.start();
//    }

    /**
     * Stop automatic running
     */
    private void stop() {
        running = false;
    }

    /**
     * Step the simulation, effectively moving the simulation forward
     */

    /**
     * Reset the simulation to initial state
     */
    private void reset() {
        stop();
        instructionModel.clear();
        memoryModel.clear();
        registerModel.clear();

    }

    private void renderInstructions() {
        instructionModel.clear();
        for (Instruction i : instructions) {
            if (hexadecimal) {
                instructionModel.addElement(i.hexRepresentation());
            } else {
                instructionModel.addElement(i.getRepresentation() + ": " + i.getEncoded());

            }
        }

    }

    private void initializeInstructions(String filename) {
        String line;
        BufferedReader reader = null;
        instructions = new ArrayList<Instruction>();

        instructionModel.clear();

        try {
            reader = new BufferedReader(new FileReader(filename));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            int i = 0;
            while ((line = reader.readLine()) != null) {
                i++;
                if (line.length() == 0) {
                    continue;
                }
                try {
                    Instruction instruction = new Instruction(line);
                    instructions.add(instruction);
                } catch (Exception e) {
                    System.out.printf("Invalid instruction '%s' on line %d\n", line, i);
                    e.printStackTrace();

                }
            }
        } catch (IOException e) {
            System.out.printf("File reading error: %s \n", e.getMessage());
            e.printStackTrace();
        }

    }

    private void renderComponents(ArrayList<String> components) {
        for (String component : components) {
            registerModel.addElement(component + "\n");
        }
    }

    private void renderRegisters() {
//        memoryModel.clear();
        for(Map.Entry<String, Integer> entry : RegisterTable.getRegisters().entrySet()) {
            String key = entry.getKey();
            int value = entry.getValue();
             try {
                 if (!RegisterFile.getRegisters().isEmpty()) {
                     memoryModel.addElement(key + ": " + RegisterFile.getRegisters().get(value).toString());
                 }
             } catch (Exception e) {
                 System.out.println("sdsdsdsd");
                 return;
             }

        }


    }



    /**
     * Load mips assembly instructions from a file and feed them into the processor
     *
     * @param filename where to parse the instructions from
     */
    private void load(String filename) {


        RegisterTable.initialize();
        ArrayList<ArrayList<Link>> instructions = null;
        try {
            instructions = InstructionParser.parse(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ArrayList<Link> initialInputs = new ArrayList<>();
        for (int i = 0; i < instructions.size(); ++i) {
            initialInputs.addAll(instructions.get(i));
        }


        Register pc = new Register("pc", "32X32");

        Link[] pcIn = new Link[32];
        for (int i = 0; i < 32; ++i)
            pcIn[i] = Simulator.falseLogic;

        Adder adder = new Adder("ADDER", "64X32");
        for (int i = 0; i < 32; ++i) {
            adder.addInput(pc.getOutput(i));
        }

        Link[] adderIn = new Link[32];
        for (int i = 0; i < 29; ++i)
            adderIn[i] = Simulator.falseLogic;
        adderIn[29] = Simulator.trueLogic;
        adderIn[30] = Simulator.falseLogic;
        adderIn[31] = Simulator.falseLogic;

        adder.addInput(adderIn);


        Mux2x1[] select = new Mux2x1[32];
        for (int i = 0; i < 32; ++i)
            select[i] = new Mux2x1("MUX" + i, "3X1");

        DFlipFlop[] shift = new DFlipFlop[2];
        shift[0] = new DFlipFlop("SH0", "2X2", ClockProvider.clock.getOutput(0), Simulator.falseLogic);
        shift[1] = new DFlipFlop("SH1", "2X2", ClockProvider.clock.getOutput(0), shift[0].getOutput(0));

        for (int i = 0; i < 32; ++i) {
            select[i].addInput(shift[1].getOutput(0), adder.getOutput(i), pcIn[i]);
        }


        ByteMemory instructionMemory = new ByteMemory("InstMem");
        SignExtend signExtend = new SignExtend("Offset", "16X32");
        ControlUnit controlUnit = new ControlUnit("cu", "6X9");
        ALUControlUnit aluControlUnit = new ALUControlUnit("acu", "8X3");
        DestSelector destSelector = new DestSelector("destSel", "11X5");
        RegisterFile registerFile = new RegisterFile("regFile", "48X64");
        LeftShifter leftShifter = new LeftShifter("Left Shifter", "37X32");
        Adder branchTargetAdder = new Adder("Branch Target", "64X32");
        DataSelector aluInputSelector = new DataSelector("ALU Source", "65X32");
        ALU alu = new ALU("alu", "67X33");
        Shifter shifter = new Shifter("shifter", "40X33");
        ALUOutputSelector aluOutputSelector = new ALUOutputSelector("ALU Output", "67X33");
//        DataSelector pcSelector = new DataSelector("PC Source", "65X32");
        ByteMemory dataMemory = new ByteMemory("DataMem");
//        Cache dataCache = new Cache("dataCache");
        DataSelector registerDataSelector = new DataSelector("Register Data", "65X32");

        And zero = new And("pcSrc", controlUnit.getOutput(6), aluOutputSelector.getOutput(0));


        for (int i = 0; i < 32; ++i) {
            Mux2x1 mux2x1 = new Mux2x1("mux", "3X1", zero.getOutput(0), select[i].getOutput(0), branchTargetAdder.getOutput(i));
            pc.addInput(mux2x1.getOutput(0));
        }



        // initializing the instruction memory with instructions
        Boolean[][] temp = new Boolean[65536][8];
        for (int i = 0; i < 65536; ++i) {
            for (int j = 0; j < 8; ++j) {
                temp[i][j] = false;
            }
        }

        for (int i = 0; i < initialInputs.size() / 8; ++i) {
            for (int j = 0; j < 8; ++j) {
                temp[i][j] = initialInputs.get(i * 8 + j).getSignal();
            }
        }

        instructionMemory.setMemory(temp);



        instructionMemory.addInput(Simulator.falseLogic);
        // set inputs for instruction memory, the 16 less significant bits used
        for (int i = 0; i < 16; ++i) {
            instructionMemory.addInput(pc.getOutput(i + 16));
        }

        for (int i = 0; i < 32; ++i) {
            instructionMemory.addInput(Simulator.falseLogic);
        }

        // sign extending 16 less significant bits of the instruction
        for (int i = 0; i < 16; ++i) {
            signExtend.addInput(instructionMemory.getOutput(i + 16));
        }

        // left shift the offset by two bits
        for (int i = 0; i < signExtend.getOutputSize(); ++i) {
            leftShifter.addInput(signExtend.getOutput(i));
        }

        leftShifter.addInput(Simulator.falseLogic);
        leftShifter.addInput(Simulator.falseLogic);
        leftShifter.addInput(Simulator.falseLogic);
        leftShifter.addInput(Simulator.trueLogic);
        leftShifter.addInput(Simulator.falseLogic);

        // calculating branch target address using an adder
        for (int i = 0; i < 32; ++i) {
            branchTargetAdder.addInput(adder.getOutput(i));
        }

        for (int i = 0; i < 32; ++i) {
            branchTargetAdder.addInput(leftShifter.getOutput(i));
        }

        // control unit
        for (int i = 0; i < 6; ++i) {
            controlUnit.addInput(instructionMemory.getOutput(i));
        }


        // alu control unit
        aluControlUnit.addInput(controlUnit.getOutput(7));
        aluControlUnit.addInput(controlUnit.getOutput(8));
        for (int i = 0; i < 6; ++i) {
            aluControlUnit.addInput(instructionMemory.getOutput(i + 26));
        }

        // register file destination selector(between rt and rd)
        destSelector.addInput(controlUnit.getOutput(0));
        for (int i = 11; i <= 20; ++i) {
            destSelector.addInput(instructionMemory.getOutput(i));
        }

        // reading data from register file
        for (int i = 6; i < 16; ++i) { // add rs and rt
            registerFile.addInput(instructionMemory.getOutput(i));
        }

        for (int i = 0; i < destSelector.getOutputSize(); ++i) { // add write register
            registerFile.addInput(destSelector.getOutput(i));
        }

        for (int i = 0; i < 32; ++i) { // add write data
            registerFile.addInput(registerDataSelector.getOutput(i));
        }

        registerFile.addInput(controlUnit.getOutput(3)); // add reg write

        // determine the second input of the alu, whether coming from register file or the immediate from the instruction
        aluInputSelector.addInput(controlUnit.getOutput(1));
        for (int i = 0; i < 32; ++i) {
            aluInputSelector.addInput(registerFile.getOutput(i + 32)); // the second output from regFile
        }

        for (int i = 0; i < 32; ++i) {
            aluInputSelector.addInput(signExtend.getOutput(i));
        }


        // alu and shift
        for (int i = 0; i < 3; ++i) {
            alu.addInput(aluControlUnit.getOutput(i));
        }

        for (int i = 0; i < 32; ++i) {
            alu.addInput(registerFile.getOutput(i));
        }

        for (int i = 0; i < 32; ++i) {
            alu.addInput(aluInputSelector.getOutput(i));
        }

        for (int i = 0; i < 32; ++i) {
            shifter.addInput(registerFile.getOutput(i + 32));
        }

        Or shamt = new Or("shamt");
        for (int i = 21; i <= 25; ++i) {
            shifter.addInput(instructionMemory.getOutput(i));
            shamt.addInput(instructionMemory.getOutput(i));
        }

        for (int i = 0; i < 3; ++i) {
            shifter.addInput(aluControlUnit.getOutput(i));
        }

        Not[] notOpcode = new Not[6];
        for (int i = 0; i < 6; ++i) {
            notOpcode[i] = new Not(String.format("not_%d", i), instructionMemory.getOutput(i));
        }

        And isShift = new And("shift");
        isShift.addInput(shamt.getOutput(0));
        for (int i = 0; i < 6; ++i) {
            isShift.addInput(notOpcode[i].getOutput(0));
        }

        // decide whether the output of arithmetic instruction should come from Shifter or ALU
        aluOutputSelector.addInput(isShift.getOutput(0));
        for (int i = 0; i < 33; ++i) {
            aluOutputSelector.addInput(alu.getOutput(i));
        }

        for (int i = 0; i < 33; ++i) {
            aluOutputSelector.addInput(shifter.getOutput(i));
        }


        // data memory
        dataMemory.addInput(controlUnit.getOutput(5));
        for (int i = 0; i < 16; ++i) {
            dataMemory.addInput(aluOutputSelector.getOutput(i + 17)); // i + 17 because the first bit of alu output is the zero bit
        }

        for (int i = 0; i < 32; ++i) {
            dataMemory.addInput(registerFile.getOutput(i + 32));
        }

        // determine the value to write into the write register
        registerDataSelector.addInput(controlUnit.getOutput(2));
        for (int i = 0; i < 32; ++i) {
            registerDataSelector.addInput(aluOutputSelector.getOutput(i + 1));
        }

        for (int i = 0; i < 32; ++i) {
//            registerDataSelector.addInput(dataCache.getOutput(i));
            registerDataSelector.addInput(dataMemory.getOutput(i));
        }


        Simulator.debugger.addTrackItem(pc, instructionMemory, controlUnit, aluControlUnit, registerFile, aluOutputSelector, dataMemory);
//        Simulator.debugger.addTrackItem(instructionMemory, signExtend, branchTargetAdder, controlUnit, aluControlUnit, destSelector, registerFile, alu);
        Simulator.debugger.setDelay(200);
        Simulator.controller.startCircuit();

    }

    private GUI.GUIListener listener = new GUI.GUIListener() {

        @Override
        public void onStop() {
            stop();
            refresh();
        }

        @Override
        public void onRun(String filename) {
            load(filename);
        }

        @Override
        public void onReset() {
            reset();
            refresh();
        }

        @Override
        public void onLoad(String filename) {
            initializeInstructions(filename);
            renderInstructions();
        }

        @Override
        public void onHex() {
            hexadecimal = true;
            renderInstructions();
            refresh();
        }

        @Override
        public void onDec() {
            hexadecimal = false;
            renderInstructions();
            refresh();
        }


    };

    public void addNode(Node node) {
        if (node instanceof Memory) {
            memories.add((Memory) node);
        }

        if (node instanceof ByteMemory) {
            byteMemories.add((ByteMemory) node);
        }

        if(node instanceof DataStream) {
            dataStreams.add((DataStream) node);
        }

        if(node instanceof Explicit || node instanceof Clock || node instanceof BigClock) {
            netList.get(0).add(node);
        }

        if (node instanceof Clock) {
            clocks.add((Clock) node);
        }

        if (node instanceof BigClock) {
            bigClocks.add((BigClock) node);
        }
    }

    public void startCircuit() {
        removeDataStream();
        saveMemoryState();
        removeLoop();
        initializeNetList();
        addLoop();
        startClocks();
        Simulator.debugger.startDebugger();
        thread.start();
    }

    public void startCircuit(int clockCount) {
        removeDataStream();
        saveMemoryState();
        removeLoop();
        initializeNetList();
        addLoop();
        startClocks();
        Simulator.debugger.startDebugger();
        thread.start();
        this.clockCount = clockCount * 2;
    }

    private void saveMemoryState() {
        for (ByteMemory mem: byteMemories) {
            for (Link in: mem.getInputs()) {
                mem.getMemIn().add(in);
            }
        }

        for (Memory mem: memories) {
            for (Link in: mem.getInputs()) {
                mem.getMemIn().add(in);
            }
        }
    }

    private void removeDataStream() {
        int m = 0;
        outer: while (!dataStreams.isEmpty()) {
            if (m >= dataStreams.size()) {
                m = 0;
            }
            DataStream dataStream = dataStreams.get(m);
            for (int l = 0; l < dataStream.getInputs().size(); ++l) {
                if (dataStream.getInput(l).getSource() instanceof DataStream) {
                    m ++;
                    continue outer;
                }
            }
            for (int j = 0; j < dataStream.getOutputs().size(); ++j) {
                Link input = dataStream.getInput(j);
                Link output = dataStream.getOutput(j);
                Node source = input.getSource();
                int sourceIndex = source.getOutputs().indexOf(input);
                if (output.getDestinations().isEmpty()) {
                    source.getOutput(sourceIndex).getDestinations().remove(dataStream);
                } else {
                    for (int k = 0; k < output.getDestinations().size(); ++k) {
                        Node destination = output.getDestination(k);
                        int destinationIndex = destination.getInputs().indexOf(output);
                        source.getOutput(sourceIndex).getDestinations().remove(dataStream);
                        source.getOutput(sourceIndex).getDestinations().add(destination);
                        destination.getInputs().set(destinationIndex, source.getOutput(sourceIndex));
                    }
                }
                dataStream.setOutput(j, source.getOutput(sourceIndex));
            }
            dataStreams.remove(dataStream);
        }
    }

    private void startClocks() {
        for (Clock clock: clocks) {
            clock.startClock();
        }
    }

    private void addLoop() {
        for (Link link: removed.keySet()) {
            for (Node node: removed.get(link)) {
                node.addInput(link);
            }
        }
    }

    private Boolean depthFirstSearch(Node node) {
        Stack<simulator.control.StackFrame> stack = new Stack<>();
        stack.push(new simulator.control.StackFrame(node));
        simulator.control.StackFrame.returnValue = false;

        outer: while (stack.size() > 0) {
            Node thisNode = stack.peek().node;

            if (simulator.control.StackFrame.returnValue || !thisNode.getLoop()) {
                thisNode.setVisited(false);
                stack.pop();
                if (stack.size() > 0)
                    stack.peek().j++;
                continue;
            }

            thisNode.setVisited(true);

            while (stack.peek().i < thisNode.getOutputs().size()) {
                while (stack.peek().j < thisNode.getOutput(stack.peek().i).getDestinations().size()) {
                    if (thisNode.getOutput(stack.peek().i).getDestination(stack.peek().j).isVisited()) {
                        if (!removed.containsKey(thisNode.getOutput(stack.peek().i))) {
                            removed.put(thisNode.getOutput(stack.peek().i), new ArrayList<>());
                        }

                        removed.get(thisNode.getOutput(stack.peek().i)).add(thisNode.getOutput(stack.peek().i).getDestination(stack.peek().j));
                        thisNode.getOutput(stack.peek().i).getDestination(stack.peek().j).getInputs().remove(thisNode.getOutput(stack.peek().i));
                        thisNode.getOutput(stack.peek().i).getDestinations().remove(stack.peek().j);
                        thisNode.setVisited(false);
                        simulator.control.StackFrame.returnValue = true;
                        stack.pop();
                        continue outer;
                    }

                    stack.push(new simulator.control.StackFrame(thisNode.getOutput(stack.peek().i).getDestination(stack.peek().j)));
                    continue outer;
                }

                stack.peek().i++;
                stack.peek().j = 0;
            }

            thisNode.setVisited(false);
            thisNode.setLoop(false);
            stack.pop();
            if (stack.size() > 0)
                stack.peek().j++;
        }

        return simulator.control.StackFrame.returnValue;
    }

    private void removeLoop() {
        boolean loopDetected = true;

        while (loopDetected) {
            loopDetected = false;
            StackFrame.returnValue = false;
            for (Node node: netList.get(0)) {
                if (depthFirstSearch(node)) {
                    loopDetected = true;
                    break;
                }
            }
        }
    }

    private void initializeNetList() {
        int level = 0;
        while (netList.size() >= level + 1) {
            initializeLevel(level++);
        }
    }

    private void initializeLevel(int level) {
        for (Node node: netList.get(level)) {
            for (Link link: node.getOutputs()) {
                link.setValidity(true);
            }
        }

        for (Node node: netList.get(level)) {
            for (Link link: node.getOutputs()) {
                for (Node innerNode : link.getDestinations()) {
                    if (!innerNode.getLatch() && !node.getLatchValidity()) {
                        continue;
                    }

                    if (innerNode.getLatch()) {
                        boolean latchValid = true;
                        for (Link inputLink : innerNode.getInputs()) {
                            if (!inputLink.getSource().getLatchValidity()) {
                                latchValid = false;
                                break;
                            }
                        }

                        innerNode.setLatchValidity(latchValid);
                    }

                    boolean valid = true;
                    for (Link innerLink : innerNode.getInputs()) {
                        if (!innerLink.isValid()) {
                            valid = false;
                            break;
                        }
                    }

                    if (valid || innerNode.getLatch()) {
                        if (netList.size() < level + 2) {
                            netList.add(new ArrayList<>());
                        }

                        if (!netList.get(level + 1).contains(innerNode)) {
                            netList.get(level + 1).add(innerNode);
                        }
                    }
                }
            }
        }
    }

    private void evaluateNetList() {
        for (List<Node> list: netList) {
            for (Node node : list) {
                node.evaluate();
            }
        }
    }

    private void toggleBigClocks() {
        for (BigClock bigClock : bigClocks) {
            bigClock.toggle();
        }
    }

//    @Override
//    public void run() {
//        while (running) {
//            evaluateNetList();
//            ArrayList<String> results = Simulator.debugger.run("gui");
//            renderComponents(results);
//        }
//
//    }

    @Override
    public void run() {
        System.out.println("here");
        int count = 0;
        while ((count < clockCount || clockCount == -1) && running) {
            toggleBigClocks();
            evaluateNetList();
            ArrayList<String> results = Simulator.debugger.run("gui");
            renderComponents(results);
            count ++;
        }
    }

    public static void main(String[] args) {
        new Controller();
    }

}
